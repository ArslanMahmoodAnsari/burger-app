import * as actionTypes from '../actions/actionTypes';
import { updateObject } from './../utility';

const INGREDIENTS_PRICE = {
    salad: 40,
    cheese: 45,
    meat: 200,
    bacon: 150
};

const initialState = {
    ingredients: null,
    totalPrice: 50,
    error: false
};

const addIngredient = (state, action) =>{

    const updatedIngredient = { [action.ingredientName] : state.ingredients[action.ingredientName] + 1 };
    const updatedIngredients = updateObject(state.ingredients, updatedIngredient);
    const updatedState = {
        ingredients: updatedIngredients,
        totalPrice: state.totalPrice + INGREDIENTS_PRICE[action.ingredientName]
    };
    return updateObject(state, updatedState);
};

const removeIngredient = (state, action) =>{
    const updatedIngredient = { [action.ingredientName] : state.ingredients[action.ingredientName] - 1 };
    const updatedIngredients = updateObject(state.ingredients, updatedIngredient);
    const updatedState = {
        ingredients: updatedIngredients,
        totalPrice: state.totalPrice - INGREDIENTS_PRICE[action.ingredientName]
    };
    return updateObject(state, updatedState);
};

const setIngredient = (state, action) =>{
    const updatedState = {
        ingredients: action.ingredients,
        totalPrice: 50,
        error: false
    }
    return updateObject(state, updatedState);
};

const fetchIngredientsFailed = (state, action) =>{
    return updateObject(state, {error: true});
};

const reducer = (state = initialState, action) => {
    switch(action.type){
        case actionTypes.ADD_INGREDIENT: return addIngredient(state, action);
        case actionTypes.REMOVE_INGREDIENT: return removeIngredient(state, action);
        case actionTypes.SET_INGREDIENT: return setIngredient(state, action);
        case actionTypes.FETCH_INGREDIENTS_FAILED: return fetchIngredientsFailed(state, action);
        default: return state;
    }
}

export default reducer;