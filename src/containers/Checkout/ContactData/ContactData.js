import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from './../../../components/UI/Button/Button';
import Spinner from './../../../components/UI/Spinner/Spinner';
import styles from './ContactData.css';
import axios from './../../../axios-order';
import Input from "./../../../components/UI/Input/Input";
import withErrorHandler from './../../../hoc/ErrorHandler/ErrorHandler';
import * as action from './../../../store/actions/index';

class ContactData extends Component{
    state = {
        orderForm: {
            name: {
                elementType: 'input',
                elementConfig:{
                    type: 'text',
                    placeholder: 'Your Name'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            street: {
                elementType: 'input',
                elementConfig:{
                    type: 'text',
                    placeholder: 'Street'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            zipCode: {
                elementType: 'input',
                elementConfig:{
                    type: 'text',
                    placeholder: 'ZIP Code'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 5,
                    maxLength: 5
                },
                valid: false,
                touched: false
            },
            country: {
                elementType: 'input',
                elementConfig:{
                    type: 'text',
                    placeholder: 'Country'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            email: {
                elementType: 'input',
                elementConfig:{
                    type: 'email',
                    placeholder: 'Your Email'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            deliveryMethod: {
                elementType: 'select',
                elementConfig:{
                    options: [
                        {value: 'fastest', displayValue: 'fastest' },
                        {value: 'cheapest', displayValue: 'cheapest' }
                    ]
                },
                validation: {},
                valid: true,
                value: 'fastest'
            }
        },
        isFormValid: false
    }

    validityCheck = (value, rules) => {
        let isValid = true;
        if(rules){
            if(rules.required){
                isValid = value.trim() !== '' && isValid;
            };
    
            if(rules.minLength){
                isValid = value.trim().length >= rules.minLength && isValid;
            };
    
            if(rules.maxLength){
                isValid = value.trim().length <= rules.maxLength && isValid;
            };    
        }
        return isValid;
    }
    orderHandler = (event) => {
        // not to reload the page
        event.preventDefault();
        const formData = {};
        for (let elemIdentifier in this.state.orderForm){
            formData[elemIdentifier] = this.state.orderForm[elemIdentifier].value;
        }
        const order = {
            ingredients: this.props.ings,
            price: this.props.price,
            orderData: formData
        };
        this.props.onOrderBurger(order);
    }

    changeHandler = (event, elementIdentifier) => {
        // the reason we are using the spread operator two time is because in javascript 
        // if we assgin an object to another its posinter is passed instead of values,
        // means it will not deep clone the nested objects and that would mutate the original state on change.

        const updatedForm = {
            ...this.state.orderForm
        }

        const updatedElement = {
            ...updatedForm[elementIdentifier]
        }
        updatedElement.value = event.target.value;
        updatedElement.valid = this.validityCheck(updatedElement.value, updatedElement.validation);
        updatedElement.touched = true;
        updatedForm[elementIdentifier] = updatedElement;
        
        let formIsValid = true;
        for (let elemIdentifier in updatedForm){
            formIsValid = updatedForm[elemIdentifier].valid && formIsValid;
        }
        this.setState({orderForm: updatedForm, isFormValid: formIsValid});
    }

    render(){
        const fromElementArray = [];
        for (let key in this.state.orderForm){
            fromElementArray.push({
                id: key,
                config: this.state.orderForm[key]
            });
        };

        let form = (
            <form onSubmit={this.orderHandler}>
                {
                    fromElementArray.map( elem => (
                        <Input
                        invalid={!elem.config.valid}
                        shouldValidate={elem.config.validation}
                        key={elem.id} 
                        elementType={elem.config.elementType} 
                        elementConfig={elem.config.elementConfig} 
                        value={elem.config.value}
                        changed={(event) => this.changeHandler(event, elem.id)}
                        touched={elem.config.touched} />
                    ))
                }
                <Button btnType='Success' disabled={!this.state.isFormValid}>ORDER</Button>
            </form>
        );
        if(this.props.loading){
            form = <Spinner />
        }
        return(
            <div className={styles.ContactData}>
                <h4>Enter Your Contact Data</h4>
                {form}       
            </div>
        );
    }
}

const mapStateToProps = state => {
    return{
        ings: state.burgerBuilder.ingredients,
        price: state.burgerBuilder.totalPrice,
        loading: state.order.loading
    };
};

const mapDispatchToProps = dispatch => {
    return{
        onOrderBurger: (orderData) => {dispatch(action.purchaseBurger(orderData))}
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(ContactData, axios));