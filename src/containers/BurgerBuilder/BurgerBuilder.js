import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import axios from './../../axios-order';
import Burger from './../../components/Burger/Burger';
import BurgerControls from './../../components/Burger/BurgerControls/BurgerControls';
import Modal from './../../components/UI/Modal/Modal';
import OrderSummary from './../../components/Burger/OrderSummary/OrderSummary';
import Spinner from './../../components/UI/Spinner/Spinner';
import errorHandler from './../../hoc/ErrorHandler/ErrorHandler';
import * as actions from './../../store/actions/index';

class BurgerBuilder extends Component{
    state = {
        buy: false,
    };

    componentDidMount () {
        this.props.onInitIngredients();   
    };
    
    orderHandler = () => {
        this.setState({buy: true});
    };

    orderCancelHandler = () => {
        this.setState({buy: false});
    };

    orderContinueHandler = () => {
        this.props.onInitPurchase();        
        this.props.history.push('/checkout');
    };
    
    updatePurchaseState = (ingredients) => {
        const sum = Object.keys(ingredients)
            .map(igKey => {
                return ingredients[igKey];
            })
            .reduce((sum, el) => {
                return sum + el;
            }, 0)
        return sum > 0;
    };

    render(){
        let disabled = {...this.props.ings};
        
        for (let key in disabled){
            disabled[key] = disabled[key] <= 0;
        };
        let orderSummary = null;
        let burger = this.props.error ? <p>can't load ingredients</p> : <Spinner />;

        if(this.props.ings){ 
            burger = (
                <Fragment>
                    <Burger ingredients={this.props.ings}/>                
                    <BurgerControls 
                    addIngredient={this.props.onIngredientAdded}
                    removeIngredient={this.props.onIngredientRemoved}                
                    price={this.props.totalPrice}
                    disabled={disabled}
                    purchasable={this.updatePurchaseState(this.props.ings)}
                    ordered={this.orderHandler}/>
                </Fragment>
            );     
            orderSummary = <OrderSummary 
            ingredients={this.props.ings}
            orderCancel={this.orderCancelHandler}
            orderCont={this.orderContinueHandler}
            price={this.props.totalPrice}/>
        }

        return(
            <Fragment>
                <Modal show={this.state.buy} modalClosed={this.orderCancelHandler}> 
                    {orderSummary}
                </Modal>
                {burger}
            </Fragment>
        )
    }
};

const mapStateToProps = state => {
    return{
        ings: state.burgerBuilder.ingredients,
        totalPrice: state.burgerBuilder.totalPrice,
        error: state.burgerBuilder.error,
        purchased: state.order.purchased
    }
};

const mapDispatchToProps = dispatch => {
    return{
        onIngredientAdded: (ingName) => dispatch(actions.addIngredient(ingName)),
        onIngredientRemoved: (ingName) => dispatch(actions.removeIngredient(ingName)),
        onInitIngredients: () => dispatch(actions.initIngredient()),
        onInitPurchase: () => dispatch(actions.purchaseInit())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(errorHandler(BurgerBuilder, axios));