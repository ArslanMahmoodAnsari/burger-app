import React from 'react';
import styles from './Burger.css';
import BurgerIngredient from './BurgerIngredient/BurgerIngredient';

const Burger = (props) => {
    let ingredients = Object.keys(props.ingredients)
    .map(ingredientKey => {
        return [...Array(props.ingredients[ingredientKey])].map((_, i) => {
             return <BurgerIngredient key={ingredientKey+i} types={ingredientKey}/>
        })
    }).reduce((arr, el) => {
        return arr.concat(el);
    }, []);
    if(ingredients.length === 0){
        ingredients = <p>please start adding ingredients</p>
    }

     return(
         <div className={styles.Burger}>
             <BurgerIngredient types="bread-top" />
             {ingredients}
             <BurgerIngredient types="bread-bottom" />
         </div>
     )
};

export default Burger;