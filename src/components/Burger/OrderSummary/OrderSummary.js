import React, {Fragment} from 'react';
import Button from './../../UI/Button/Button';

const OrderSummary = (props) => {
    const ingredientSummary = Object.keys(props.ingredients)
        .map(igKey => {
        return (
            <li key={igKey}>
                <span>{igKey}</span>: {props.ingredients[igKey]}
            </li>);
        });

    return (
        <Fragment>
            <h3>Your Order</h3>
            <p>Your Burger Contains: </p>
            <ul>
                {ingredientSummary}
            </ul>
            <p><strong>Total price: {props.price}</strong></p>
            <p>Continue to checkout?</p>
            <Button
            clicked={props.orderCont}
            btnType={"Success"}>
                CONTINUE
            </Button>
            <Button
            clicked={props.orderCancel}
            btnType={"Danger"}>
                CANCEL
            </Button>
        </Fragment>
    )
}

export default OrderSummary
