import React from 'react';
import style from './BurgerControl.css';

const BurgerControl = (props) => { 
    return (
        <div className={style.BurgerControl}>
            <div className={style.Label}>{props.label}</div>
            <button 
            className={style.Less} 
            onClick={props.removed} 
            disabled={props.disabled}>Less</button>
            <button 
            className={style.More} 
            onClick={props.added}>More</button>
        </div>
    )
}

export default BurgerControl
