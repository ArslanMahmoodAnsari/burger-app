import React from 'react';
import BurgerControl from './BurgerControl/BurgerControl';
import styles from './BurgerControls.css';

const controls = [
    {label: 'Cheese', type: 'cheese'},
    {label: 'Salad', type: 'salad'},
    {label: 'Bacon', type: 'bacon'},
    {label: 'Meat', type: 'meat'}
]
const BurgerControls = (props) => (
    <div className={styles.BurgerControls}>
        <p>Current Price: <strong>{props.price}</strong></p>
        {
        controls.map( ctrl => (
            <BurgerControl
            key={ctrl.label}
            label={ctrl.label}
            added={()=> props.addIngredient(ctrl.type)}
            removed={()=> props.removeIngredient(ctrl.type)}
            disabled={props.disabled[ctrl.type]}/>
        ))
        }
        <button 
        className={styles.OrderButton} 
        disabled={!props.purchasable}
        onClick={props.ordered}>Order</button>
    </div>
);

export default BurgerControls;
