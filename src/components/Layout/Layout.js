import React, {Fragment, Component} from 'react';
import styles from './Layout.css';
import Toolbar from './../Navigation/Toolbar/Toolbar';
import SideDrawer from './../Navigation/SideDrawer/SideDrawer';

class layout extends Component {
    state ={
        showSideDrawer: false
    }

    sideDrawerCloseHandler = () => {
        this.setState({showSideDrawer: false});
    }

    sideDrawerOpenHandler = () => {
        this.setState((prevState) => {
            return {showSideDrawer: !prevState.showSideDrawer}
        })
    }
    render(){
    
    return(
        <Fragment>
            <Toolbar drawerToggle={this.sideDrawerOpenHandler}/>
            <SideDrawer closed={this.sideDrawerCloseHandler} show={this.state.showSideDrawer}/>
            <main className={styles.Content}>
                {this.props.children}
            </main>
       </Fragment>
    )
   };
}

export default layout;