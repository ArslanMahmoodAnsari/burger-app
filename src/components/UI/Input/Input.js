import React from "react";
import styles from "./Input.css";

const Input = (props) => {
 let inputElement = null;
 const inputStyles = [styles.InputElement];
 
 if(props.invalid && props.shouldValidate && props.touched){
     inputStyles.push(styles.Invalid);
 }    
 
 switch(props.elementType){
     case ('input'):
        inputElement = <input 
            className={inputStyles.join(' ')} 
            {...props.elementConfig} 
            value={props.value}
            onChange={props.changed} />;
        break;
    case ('textArea'):
        inputElement = <textarea 
            className={inputStyles.join(' ')} 
            {...props.elementConfig} 
            value={props.value}
            onChange={props.changed} />;
        break;
    case ('select'):
        inputElement = (
            <select 
                className={inputStyles.join(' ')} 
                value={props.value}
                onChange={props.changed}> 
                {props.elementConfig.options.map(opt => (
                    <option key={opt.value} value={opt.value}>
                        {opt.displayValue}
                    </option>
                ))} 
            </select>
        );
        break;
    default:
        inputElement = <input
            className={inputStyles.join(' ')} 
            {...props.elementConfig} 
            value={props.value}
            onChange={props.changed} />; 
 }
 return(
    <div className={styles.Input}>
        <label className={styles.Label}>{props.label}</label>
        {inputElement}
    </div>    
 );
}

export  default Input;