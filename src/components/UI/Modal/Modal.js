import React, { Fragment, useEffect} from 'react';
import Backdrop from './../Backdrop/Backdrop';

import style from './Modal.css';

const Modal = (props) =>{
    useEffect(()=> {
    }, [props.show]);
    return (
        <Fragment>
            <Backdrop show={props.show} modalClosed={props.modalClosed}/>
            <div 
            className={style.Modal}
            style={{
                transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                opacity: props.show ? '1' : '0'
            }}>
                {props.children}
            </div>
        </Fragment>
    )
}

export default React.memo(Modal);
