import React, { Fragment } from 'react';
import Logo from './../../Logo/Logo';
import NavigationItems from './../NavigationItems/NavigationItems';
import Backdrop from './../../UI/Backdrop/Backdrop';
import styles from "./SideDrawer.css";

const SideDrawer = (props) => {
    let dynamicStyles = [styles.SideDrawer, styles.Close];
    if(props.show){
        dynamicStyles = [styles.SideDrawer, styles.Open];
    }

    return (
        <Fragment>
        <Backdrop show={props.show} modalClosed={props.closed}/>
        <div className={dynamicStyles.join(' ')}>
            <div className={styles.Logo}>
            <Logo />
            </div>
            <nav>
                <NavigationItems />
            </nav>
        </div>
        </Fragment>
        
    )
}

export default SideDrawer;
