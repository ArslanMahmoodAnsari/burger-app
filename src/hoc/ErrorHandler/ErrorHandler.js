import React, { Component, Fragment } from 'react';
import Modal from './../../components/UI/Modal/Modal';



const ErrorHandler = (WrappedComponent, axios) =>  {
    return class extends Component {
        state = {
            error: null
        }
        constructor(){
            super();
            this.reqInterceptors = axios.interceptors.request.use(req => {
                this.setState({error: null})
                return req;
            })

            this.resInterceptors = axios.interceptors.response.use(res => res , error => {
                this.setState({error: error});
            })
        }
        componentWillUnmount(){
            axios.interceptors.request.eject(this.reqInterceptors);
            axios.interceptors.response.eject(this.resInterceptors);
        }

        errorAcknowledgedHandler = () => {
            this.setState({error: null});
        }

        render(){
            return(
                <Fragment>
                 <Modal 
                  show={this.state.error}
                  modalClosed={this.errorAcknowledgedHandler}>
                    {this.state.error ? this.state.error.message : null}
                </Modal>
                 <WrappedComponent {...this.props} />   
                </Fragment>
            )
        }
    }
}

export default ErrorHandler;
